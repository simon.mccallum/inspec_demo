FROM chef/inspec

# Handle GitLab's choice of repo location.
RUN mkdir -p /builds/iqa_public/inspec_demo/

# Fix "bug" in chef/inspec of not having user 1000.
# Every image needs this user because all volumes mounted into
# a container by docker-compose are owned by user 1000.
RUN adduser -s /sbin/nologin -D -h /builds/iqa_public/inspec_demo/ -u 1000 inspec
RUN chown inspec /builds/iqa_public/inspec_demo/
USER inspec
